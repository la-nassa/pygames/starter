# Starter

![Screenshot of gameplay](sshots/pysurfv3.gif)

A small and easy surfing game made following the [Primer on PyGame at Realpython](https://realpython.com/pygame-a-primer/)

Nothing fancy, I misinterpreted the word surf (meaning actually surface) and thought of a surf board, then used that for the game and added fish and sharks as enemies.
