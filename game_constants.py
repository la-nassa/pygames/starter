import os
# Updated to conform to flake8 and black standards
from pygame.locals import (
    RLEACCEL,
    K_UP,
    K_DOWN,
    K_LEFT,
    K_RIGHT,
    K_ESCAPE,
    KEYDOWN,
    QUIT,
)

### CONSTANTS
SCREEN_WIDTH = 1000
SCREEN_HEIGHT = 700
PLAYER_SPEED = 10
ENEMY_SPEED_MIN = 4
ENEMY_SPEED_MAX = 30
ENEMY_SPAWNING_TIME = 350
ENEMY_SPEED_RANDOMNESS = 7 # controls how many different speed can be given
ENEMY_IMAGES_RANDOMNESS = 4 # controls how many different image can be used
assert(ENEMY_SPEED_MAX-ENEMY_SPEED_MIN > ENEMY_SPEED_RANDOMNESS)
HEADERBAR_HEIGHT = 50
HEADERBAR_COLOR = (177, 244, 251)
WAVE_SPEED_MIN = 5
WAVE_SPEED_MAX = 6
WAVE_SPAWNING_TIME = 4550
BACKGROUND_COLOR = (31, 76, 181)
GAME_DIFFICULTY = 3 # it controls how fast the game becomes more difficult (each 20 - 5*GAME_DIFFICULTY enemies, min each enemy)
WINNING_LEVEL = 25 # game will be won at this gameLevel
WITH_WAVES = True

ASSET_FOLDER = "assets"
MUSIC_FOLDER = os.path.join(ASSET_FOLDER, "music")
IMG_FOLDER = os.path.join(ASSET_FOLDER, "img")
ENEMIES_FOLDER = os.path.join(IMG_FOLDER, "enemies")
ENEMIES_NAME = ["jelly_18pix.png", "jelly_20pix.png", "jelly_25pix.png", "yellow_fish_h20pix.png", "yellow_fish_h50pix.png",
                "shark_25pix.png", "shark_50pix.png", "dolphin_l50pix.png", "shark_100pix.png", "dolphin_l100pix.png",
                "whale_h50pix.png", "orca_h60pix.png", "capodoglio_h70pix.png", "whale_h90pix.png", "orca_h105pix.png", "capodoglio_h125pix.png"]
