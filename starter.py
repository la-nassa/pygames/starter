### IMPORTS
import pygame
import random
from game_constants import *
from game_elements import Player, Enemy, HeaderBar, Life, StartScreen, Wave
import os

### HERE THE CODE STARTS
if __name__ == "__main__":

    ### INITIALIZATION
    # Setup for sounds. Defaults are good.
    pygame.mixer.init()
    pygame.mixer.pre_init(44100, -16, 2, 2048)
    pygame.init()
    # Setup the clock for a decent framerate
    clock = pygame.time.Clock()

    ### SCREEN
    screen = pygame.display.set_mode([SCREEN_WIDTH, SCREEN_HEIGHT])
    # set the pygame window name
    pygame.display.set_caption("Surfin' in the USA")

    # Create a custom event for adding a new enemy
    # pygame defines events internally as integers,
    # so we need to add one and define the integer
    ADDENEMY = pygame.USEREVENT + 1
    # each spawning time the event is fired
    pygame.time.set_timer(ADDENEMY, ENEMY_SPAWNING_TIME)
    if WITH_WAVES:
        print("Playing with the waves!")
        print("If they are disturbing or flickers, change WITH_WAVES to False in game_constants.py")
        ADDWAVE = ADDENEMY + 1
        pygame.time.set_timer(ADDWAVE, WAVE_SPAWNING_TIME)

    ### INSTANTIATE CLASSES
    player = Player()
    # Create groups to hold enemy sprites and all sprites
    # - enemies is used for collision detection and position updates
    # - all_sprites is used for rendering
    enemies = pygame.sprite.Group()
    all_sprites = pygame.sprite.Group()
    all_sprites.add(player)

    # HeaderBar and other optical elements
    headerbar = HeaderBar()
    life1 = Life(1)
    life2 = Life(2)
    life3 = Life(3)
    all_other_sprites = pygame.sprite.Group()
    all_other_sprites.add(headerbar)
    all_other_sprites.add(life1)
    all_other_sprites.add(life2)
    all_other_sprites.add(life3)
    startscreen = StartScreen()
    waves = pygame.sprite.Group()

    # GameLevel makes it harder with the time
    gameLevel = 0
    passedEnemies = 0
    enemiesForNextLevel = max(3, 20 - 5*GAME_DIFFICULTY)
    print(f"Playing with GAME_DIFFICULTY set to {GAME_DIFFICULTY}!")
    print("If you with slower gameplay, set GAME_DIFFICULTY lower, if faster, set it higher (in game_constants.py)")

    # Load and play background music
    # Sound source: http://ccmixter.org/files/Apoxode/59262
    # License: https://creativecommons.org/licenses/by/3.0/
    sounds = []
    sounds.append(pygame.mixer.Sound(os.path.join(MUSIC_FOLDER, "goinghigher.mp3")))
    sounds.append(pygame.mixer.Sound(os.path.join(MUSIC_FOLDER, "waves.ogg")))
    for sound in sounds:
        sound.play(loops=-1)
    # for only one song
    #pygame.mixer.music.load(os.path.join(MUSIC_FOLDER, "waves.ogg"))
    #pygame.mixer.music.play(loops=-1)

    # Load all sound files
    # Sound sources: Jon Fincher
    move_sound = pygame.mixer.Sound(os.path.join(MUSIC_FOLDER, "whoosh.ogg"))
    move_sound.set_volume(0.3)
    dead_sound = pygame.mixer.Sound(os.path.join(MUSIC_FOLDER, "shot.ogg"))

    running = True
    start_screen = True
    playing = False
    lost_screen = False
    won_screen = False
    while running:

        while start_screen:
            for event in pygame.event.get():
                # KEYSTROKES
                if event.type == KEYDOWN:
                    if event.key == K_ESCAPE:
                        start_screen = False
                        playing = False
                        lost_screen = False
                        running = False
                    else:
                        start_screen = False
                        playing = True

                # QUIT
                if event.type == pygame.QUIT:
                    start_screen = False
                    playing = False
                    lost_screen = False
                    running = False

            ## DRAWING ON THE SCREEN
            screen.blit(startscreen.surf, startscreen.rect)

            # render to the window
            pygame.display.flip()

        ### BIG LOOP OF THE GAME
        while playing:
            # catch the events!
            for event in pygame.event.get():
                # distinguish event types
                # KEYSTROKES
                if event.type == KEYDOWN:
                    if event.key == K_ESCAPE:
                        start_screen = False
                        playing = False
                        lost_screen = False
                        running = False
                        won_screen = False
                # QUIT
                if event.type == pygame.QUIT:
                    start_screen = False
                    playing = False
                    lost_screen = False
                    running = False
                    won_screen = False

                # ENEMY
                if event.type == ADDENEMY:
                    new_enemy = Enemy(gameLevel) # new enemy from the class
                    enemies.add(new_enemy) # add to the sprite group
                    all_sprites.add(new_enemy) # add to all sprite for drawing

                # ENEMY
                if WITH_WAVES:
                    if event.type == ADDWAVE:
                        new_wave = Wave() # new enemy from the class
                        waves.add(new_wave) # add to all sprite for drawing
                        all_other_sprites.add(new_wave)

            # Get the set of keys pressed and check for user input
            pressed_keys = pygame.key.get_pressed()
            player.update(pressed_keys, move_sound)
            enemies_before_update = len(enemies.sprites())
            enemies.update()
            enemies_after_update = len(enemies.sprites())
            if enemies_after_update < enemies_before_update:
                passedEnemies += 1
            if passedEnemies > 0 and passedEnemies % enemiesForNextLevel == 0:
                gameLevel += 1
                passedEnemies = 0
            waves.update()

            ## DRAWING ON THE SCREEN
            screen.fill(BACKGROUND_COLOR)

            # blit transfer Surface onto another Surface (screen is a Surface)
            # Draw all sprites
            for entity in all_other_sprites:
                screen.blit(entity.surf, entity.rect)
            for entity in all_sprites:
                screen.blit(entity.surf, entity.rect)

            # Check if any enemies have collided with the player

            if pygame.sprite.spritecollideany(player, enemies):
                move_sound.stop()
                dead_sound.play()
                for enemy in enemies:
                    enemy.kill()
                if player.lives == 3:
                    player.lives = 2
                    life3.kill()
                elif player.lives == 2:
                    player.lives = 1
                    life2.kill()
                else:
                    # If so, then remove the player and stop the loop
                    life1.kill()
                    player.kill()
                    move_sound.stop()
                    startscreen.kill()
                    playing = False
                    lost_screen = True
                    print(f"Lost! You reached level: {gameLevel} out of {WINNING_LEVEL}")
                    gameLevel = 0

            if gameLevel > WINNING_LEVEL:
                playing = False
                won_screen = True
                gameLevel = 0
                player.kill()

            # render to the window
            pygame.display.flip()

            # Ensure program maintains a rate of 30 frames per second
            clock.tick(60)

        while won_screen:

            ## DRAWING ON THE SCREEN
            lost_img = pygame.image.load(os.path.join(IMG_FOLDER, "won_screen.png")).convert()
            screen.blit(lost_img, (0,0))

            for event in pygame.event.get():
                # KEYSTROKES
                if event.type == KEYDOWN:
                    if event.key == K_ESCAPE:
                        running = False
                        won_screen = False
                    else:
                        won_screen = False
                        start_screen = True
                        player = Player()
                        all_sprites.add(player)
                        all_other_sprites.add(life1)
                        all_other_sprites.add(life2)
                        all_other_sprites.add(life3)
                        startscreen = StartScreen()

                # QUIT
                if event.type == pygame.QUIT:
                    won_screen = False
                    start_screen = False
                    playing = False
                    lost_screen = False
                    running = False

            # render to the window
            pygame.display.flip()

        while lost_screen:

            ## DRAWING ON THE SCREEN
            lost_img = pygame.image.load(os.path.join(IMG_FOLDER, "lost_screen.png")).convert()
            screen.blit(lost_img, (0,0))

            for event in pygame.event.get():
                # KEYSTROKES
                if event.type == KEYDOWN:
                    if event.key == K_ESCAPE:
                        start_screen = False
                        playing = False
                        lost_screen = False
                        running = False
                    else:
                        lost_screen = False
                        start_screen = True
                        player = Player()
                        all_sprites.add(player)
                        all_other_sprites.add(life1)
                        all_other_sprites.add(life2)
                        all_other_sprites.add(life3)
                        startscreen = StartScreen()

                # QUIT
                if event.type == pygame.QUIT:
                    start_screen = False
                    playing = False
                    lost_screen = False
                    running = False

            # render to the window
            pygame.display.flip()

    pygame.mixer.music.stop()
    pygame.quit()
