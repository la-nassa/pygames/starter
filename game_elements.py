import pygame
import random
import os
from game_constants import *
import pdb
### DEFINITION OF THE NEEDED CLASSES
IMG_FOLDER = "assets/img"

# Define a Player object by extending pygame.sprite.Sprite
# The surface drawn on the screen is now an attribute of 'player'
class Player(pygame.sprite.Sprite):
    def __init__(self):
        super(Player, self).__init__()
        self.surf = pygame.image.load(os.path.join(IMG_FOLDER, "surfboard_100pix.png")).convert()
        self.surf.set_colorkey((0, 0, 0), RLEACCEL)
        #self.surf = pygame.Surface((100, 35))
        #self.surf.fill((20, 20, 200))
        self.rect = self.surf.get_rect(
            center=(
                50,
                (SCREEN_HEIGHT/2),
            )
        )
        self.lives = 3

    # Move the sprite based on user keypresses
    def update(self, pressed_keys, move_sound):
        #self.surf.set_colorkey((0, 255, 255))
        if pressed_keys[K_UP]:
            move_sound.play()
            self.rect.move_ip(0, -PLAYER_SPEED)
        if pressed_keys[K_DOWN]:
            move_sound.play()
            self.rect.move_ip(0, PLAYER_SPEED)
        if pressed_keys[K_LEFT]:
            self.rect.move_ip(-PLAYER_SPEED, 0)
        if pressed_keys[K_RIGHT]:
            self.rect.move_ip(PLAYER_SPEED, 0)

        # Keep player on the screen
        if self.rect.left < 0:
            self.rect.left = 0
        if self.rect.right > SCREEN_WIDTH:
            self.rect.right = SCREEN_WIDTH
        if self.rect.top <= HEADERBAR_HEIGHT:
            self.rect.top = HEADERBAR_HEIGHT
        if self.rect.bottom >= SCREEN_HEIGHT:
            self.rect.bottom = SCREEN_HEIGHT

# Define the enemy object by extending pygame.sprite.Sprite
# The surface you draw on the screen is now an attribute of 'enemy'
class Enemy(pygame.sprite.Sprite):
    def __init__(self, gameLevel):
        super(Enemy, self).__init__()
        enemy_random_factor = random.randint(0,ENEMY_IMAGES_RANDOMNESS)
        enemy_chosen = min(gameLevel, len(ENEMIES_NAME)-ENEMY_IMAGES_RANDOMNESS-1) + enemy_random_factor
        #print(f"enemy_chosen: {enemy_chosen}, gameLevel: {gameLevel}, enemy_random_factor: {enemy_random_factor}")
        img_name = ENEMIES_NAME[enemy_chosen]
        #print(f"chosen {img_name}")
        self.surf = pygame.image.load(os.path.join(ENEMIES_FOLDER, img_name)).convert_alpha()
        #self.surf.set_colorkey(BACKGROUND_COLOR, RLEACCEL)
        self.rect = self.surf.get_rect(
            center=(
                random.randint(SCREEN_WIDTH + 20, SCREEN_WIDTH + 100),
                random.randint(HEADERBAR_HEIGHT, SCREEN_HEIGHT),
            )
        )
        speed_random_factor = random.randint(0,ENEMY_SPEED_RANDOMNESS)
        self.speed = ENEMY_SPEED_MIN + min(gameLevel, ENEMY_SPEED_MAX-ENEMY_SPEED_MIN-ENEMY_SPEED_RANDOMNESS) + speed_random_factor
        #print(f"GameLevel: {gameLevel}, speed_random_factor: {speed_random_factor}, speed: {self.speed}")

    # Move the sprite based on speed
    # Remove the sprite when it passes the left edge of the screen
    def update(self):
        dead = 0
        self.rect.move_ip(-self.speed, 0)
        if self.rect.right < 0:
            self.kill()
            dead += 1
        return dead

class StartScreen(pygame.sprite.Sprite):
    def __init__(self):
        super(StartScreen, self).__init__()
        bg_choice = random.randint(1,3)
        if bg_choice == 1:
            img_name = "surf_bg_1.jpg"
        elif bg_choice == 2:
            img_name = "surf_bg_2.jpg"
        else:
            img_name = "surf_bg_3.jpg"
        self.surf = pygame.image.load(os.path.join(IMG_FOLDER, img_name)).convert()
        self.rect = self.surf.get_rect()

class HeaderBar(pygame.sprite.Sprite):
    def __init__(self):
        super(HeaderBar, self).__init__()
        img_name = "headerbar.png"
        self.surf = pygame.image.load(os.path.join(IMG_FOLDER, img_name)).convert()
        self.rect = self.surf.get_rect()

class Wave(pygame.sprite.Sprite):
    def __init__(self):
        super(Wave, self).__init__()
        wave_chosen = random.randint(1,2)
        if wave_chosen == 1:
            wave_name = "wave_hand_5.png"
        else: #wave_chosen == 2:
            wave_name = "wave_hand_4.png"
        self.surf = pygame.image.load(os.path.join(IMG_FOLDER, wave_name)).convert()
        self.surf.set_colorkey((0, 0, 0), RLEACCEL)
        self.rect = self.surf.get_rect(
            center=(
                SCREEN_WIDTH + SCREEN_WIDTH / 2 + 50,
                (SCREEN_HEIGHT/2) + HEADERBAR_HEIGHT,
            )
        )
        self.speed = random.randint(WAVE_SPEED_MIN, WAVE_SPEED_MAX)

    # Move the sprite based on speed
    # Remove the sprite when it passes the left edge of the screen
    def update(self):
        self.rect.move_ip(-self.speed, 0)
        if self.rect.right < 0:
            self.kill()


class Life(pygame.sprite.Sprite):
    def __init__(self, position):
        super(Life, self).__init__()
        img_name = "surf_lives.png"
        self.surf = pygame.image.load(os.path.join(IMG_FOLDER, img_name)).convert()
        self.surf.set_colorkey(HEADERBAR_COLOR, RLEACCEL)
        self.rect = self.surf.get_rect(
            center=(
                1025-50*position,
                25,
            )
        )
